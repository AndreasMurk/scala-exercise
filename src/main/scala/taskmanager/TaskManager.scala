package taskmanager

case class TaskManager(tasks: List[Task] = List()) {
  def addTask(description: String, status: TaskStatus): TaskManager = {
    val nextId = tasks match {
      case Nil => 1
      case _ => tasks.map(_.id.getOrElse(0)).max + 1
    }
    val newTask = Task(Some(nextId), description, status)
    copy(tasks = tasks :+ newTask)
  }


  def deleteTask(id: Int): Either[String, TaskManager] = {
    tasks.find(_.id.contains(id)) match {
      case Some(_) =>
        val updatedTasks = tasks.filterNot(_.id.contains(id))
        Right(copy(tasks = updatedTasks))
      case None => Left(s"Task not found: $id")
    }
  }

  def completeTask(id: Int): Either[String, TaskManager] = {
    completeTaskByPredicate(t => t.id.contains(id), s"Task not found: $id")
  }

  def completeTaskByName(name: String): Either[String, TaskManager] = {
    completeTaskByPredicate(t => t.description == name, s"Task not found: $name")
  }

  private def completeTaskByPredicate(predicate: Task => Boolean, errorMessage: String): Either[String, TaskManager] = {
    tasks.find(predicate) match {
      case Some(task) =>
        val updatedTasks = tasks.map(t => if (t.id == task.id) t.copy(status = Completed) else t)
        Right(copy(tasks = updatedTasks))
      case None => Left(errorMessage)
    }
  }

  def listTasks(status: TaskStatus): List[Task] = {
    tasks.filter(_.status == status)
  }

  def batchUpdate(updateFunction: Task => Task): TaskManager = {
    val updatedTasks = tasks.map(updateFunction)
    copy(tasks = updatedTasks)
  }
}

object TaskManager {
  // Any additional methods related to TaskManager
}